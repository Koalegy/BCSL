// ==UserScript==
// @name Bondage Club Script Loader
// @namespace https://www.bondageprojects.com/
// @version 1.20
// @description BCSL - Bondage Club Script Loader - Manage scripts without the hassle.
// @author Koalegy
// @match https://bondageprojects.elementfx.com/*
// @match https://www.bondageprojects.elementfx.com/*
// @match https://bondage-europe.com/*
// @match https://www.bondage-europe.com/*
// @match http://localhost:*/*
// @grant none
// @run-at document-end
// ==/UserScript==
// @ts-check
// eslint-disable-next-line
/// <reference path="./bcsl.d.ts" />
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable no-undef */
/* eslint-disable no-implicit-globals */
/* eslint-disable no-alert */

const BCSL_VERSION = "1.20";

/*
 * Bondage Club Mod Development Kit
 * For more info see: https://github.com/Jomshir98/bondage-club-mod-sdk
 */
/** @type {import('./types/bcModSdk').ModSDKGlobalAPI} */
// eslint-disable-next-line capitalized-comments, multiline-comment-style
// prettier-ignore
// @ts-ignore
// eslint-disable-next-line
var bcModSdk = function () { "use strict"; const e = "1.1.0"; function o(e) { alert("Mod ERROR:\n" + e); const o = new Error(e); throw console.error(o), o } const t = new TextEncoder; function n(e) { return !!e && "object" == typeof e && !Array.isArray(e) } function r(e) { const o = new Set; return e.filter((e => !o.has(e) && o.add(e))) } const i = new Map, a = new Set; function d(e) { a.has(e) || (a.add(e), console.warn(e)) } function s(e) { const o = [], t = new Map, n = new Set; for (const r of p.values()) { const i = r.patching.get(e.name); if (i) { o.push(...i.hooks); for (const [o, a] of i.patches.entries()) t.has(o) && t.get(o) !== a && d(`ModSDK: Mod '${r.name}' is patching function ${e.name} with same pattern that is already applied by different mod, but with different pattern:\nPattern:\n${o}\nPatch1:\n${t.get(o) || ""}\nPatch2:\n${a}`), t.set(o, a), n.add(r.name) } } o.sort(((e, o) => o.priority - e.priority)); const r = function (e, o) { if (0 === o.size) return e; let t = e.toString().replaceAll("\r\n", "\n"); for (const [n, r] of o.entries()) t.includes(n) || d(`ModSDK: Patching ${e.name}: Patch ${n} not applied`), t = t.replaceAll(n, r); return (0, eval)(`(${t})`) }(e.original, t); let i = function (o) { var t, i; const a = null === (i = (t = m.errorReporterHooks).hookChainExit) || void 0 === i ? void 0 : i.call(t, e.name, n), d = r.apply(this, o); return null == a || a(), d }; for (let t = o.length - 1; t >= 0; t--) { const n = o[t], r = i; i = function (o) { var t, i; const a = null === (i = (t = m.errorReporterHooks).hookEnter) || void 0 === i ? void 0 : i.call(t, e.name, n.mod), d = n.hook.apply(this, [o, e => { if (1 !== arguments.length || !Array.isArray(o)) throw new Error(`Mod ${n.mod} failed to call next hook: Expected args to be array, got ${typeof e}`); return r.call(this, e) }]); return null == a || a(), d } } return { hooks: o, patches: t, patchesSources: n, enter: i, final: r } } function c(e, o = !1) { let r = i.get(e); if (r) o && (r.precomputed = s(r)); else { let o = window; const a = e.split("."); for (let t = 0; t < a.length - 1; t++)if (o = o[a[t]], !n(o)) throw new Error(`ModSDK: Function ${e} to be patched not found; ${a.slice(0, t + 1).join(".")} is not object`); const d = o[a[a.length - 1]]; if ("function" != typeof d) throw new Error(`ModSDK: Function ${e} to be patched not found`); const c = function (e) { let o = -1; for (const n of t.encode(e)) { let e = 255 & (o ^ n); for (let o = 0; o < 8; o++)e = 1 & e ? -306674912 ^ e >>> 1 : e >>> 1; o = o >>> 8 ^ e } return ((-1 ^ o) >>> 0).toString(16).padStart(8, "0").toUpperCase() }(d.toString().replaceAll("\r\n", "\n")), l = { name: e, original: d, originalHash: c }; r = Object.assign(Object.assign({}, l), { precomputed: s(l), router: () => { }, context: o, contextProperty: a[a.length - 1] }), r.router = function (e) { return function (...o) { return e.precomputed.enter.apply(this, [o]) } }(r), i.set(e, r), o[r.contextProperty] = r.router } return r } function l() { const e = new Set; for (const o of p.values()) for (const t of o.patching.keys()) e.add(t); for (const o of i.keys()) e.add(o); for (const o of e) c(o, !0) } function f() { const e = new Map; for (const [o, t] of i) e.set(o, { name: o, original: t.original, originalHash: t.originalHash, sdkEntrypoint: t.router, currentEntrypoint: t.context[t.contextProperty], hookedByMods: r(t.precomputed.hooks.map((e => e.mod))), patchedByMods: Array.from(t.precomputed.patchesSources) }); return e } const p = new Map; function u(e) { p.get(e.name) !== e && o(`Failed to unload mod '${e.name}': Not registered`), p.delete(e.name), e.loaded = !1, l() } function g(e, t, r) { "string" == typeof e && "string" == typeof t && (alert(`Mod SDK warning: Mod '${e}' is registering in a deprecated way.\nIt will work for now, but please inform author to update.`), e = { name: e, fullName: e, version: t }, t = { allowReplace: !0 === r }), e && "object" == typeof e || o("Failed to register mod: Expected info object, got " + typeof e), "string" == typeof e.name && e.name || o("Failed to register mod: Expected name to be non-empty string, got " + typeof e.name); let i = `'${e.name}'`; "string" == typeof e.fullName && e.fullName || o(`Failed to register mod ${i}: Expected fullName to be non-empty string, got ${typeof e.fullName}`), i = `'${e.fullName} (${e.name})'`, "string" != typeof e.version && o(`Failed to register mod ${i}: Expected version to be string, got ${typeof e.version}`), e.repository || (e.repository = void 0), void 0 !== e.repository && "string" != typeof e.repository && o(`Failed to register mod ${i}: Expected repository to be undefined or string, got ${typeof e.version}`), null == t && (t = {}), t && "object" == typeof t || o(`Failed to register mod ${i}: Expected options to be undefined or object, got ${typeof t}`); const a = !0 === t.allowReplace, d = p.get(e.name); d && (d.allowReplace && a || o(`Refusing to load mod ${i}: it is already loaded and doesn't allow being replaced.\nWas the mod loaded multiple times?`), u(d)); const s = e => { "string" == typeof e && e || o(`Mod ${i} failed to patch a function: Expected function name string, got ${typeof e}`); let t = g.patching.get(e); return t || (t = { hooks: [], patches: new Map }, g.patching.set(e, t)), t }, f = { unload: () => u(g), hookFunction: (e, t, n) => { g.loaded || o(`Mod ${i} attempted to call SDK function after being unloaded`); const r = s(e); "number" != typeof t && o(`Mod ${i} failed to hook function '${e}': Expected priority number, got ${typeof t}`), "function" != typeof n && o(`Mod ${i} failed to hook function '${e}': Expected hook function, got ${typeof n}`); const a = { mod: g.name, priority: t, hook: n }; return r.hooks.push(a), l(), () => { const e = r.hooks.indexOf(a); e >= 0 && (r.hooks.splice(e, 1), l()) } }, patchFunction: (e, t) => { g.loaded || o(`Mod ${i} attempted to call SDK function after being unloaded`); const r = s(e); n(t) || o(`Mod ${i} failed to patch function '${e}': Expected patches object, got ${typeof t}`); for (const [n, a] of Object.entries(t)) "string" == typeof a ? r.patches.set(n, a) : null === a ? r.patches.delete(n) : o(`Mod ${i} failed to patch function '${e}': Invalid format of patch '${n}'`); l() }, removePatches: e => { g.loaded || o(`Mod ${i} attempted to call SDK function after being unloaded`); s(e).patches.clear(), l() }, callOriginal: (e, t, n) => (g.loaded || o(`Mod ${i} attempted to call SDK function after being unloaded`), "string" == typeof e && e || o(`Mod ${i} failed to call a function: Expected function name string, got ${typeof e}`), Array.isArray(t) || o(`Mod ${i} failed to call a function: Expected args array, got ${typeof t}`), function (e, o, t = window) { return c(e).original.apply(t, o) }(e, t, n)), getOriginalHash: e => ("string" == typeof e && e || o(`Mod ${i} failed to get hash: Expected function name string, got ${typeof e}`), c(e).originalHash) }, g = { name: e.name, fullName: e.fullName, version: e.version, repository: e.repository, allowReplace: a, api: f, loaded: !0, patching: new Map }; return p.set(e.name, g), Object.freeze(f) } function h() { const e = []; for (const o of p.values()) e.push({ name: o.name, fullName: o.fullName, version: o.version, repository: o.repository }); return e } let m; const y = function () { if (void 0 === window.bcModSdk) return window.bcModSdk = function () { const o = { version: e, apiVersion: 1, registerMod: g, getModsInfo: h, getPatchingInfo: f, errorReporterHooks: Object.seal({ hookEnter: null, hookChainExit: null }) }; return m = o, Object.freeze(o) }(); if (n(window.bcModSdk) || o("Failed to init Mod SDK: Name already in use"), 1 !== window.bcModSdk.apiVersion && o(`Failed to init Mod SDK: Different version already loaded ('1.1.0' vs '${window.bcModSdk.version}')`), window.bcModSdk.version !== e && (alert(`Mod SDK warning: Loading different but compatible versions ('1.1.0' vs '${window.bcModSdk.version}')\nOne of mods you are using is using an old version of SDK. It will work for now but please inform author to update`), window.bcModSdk.version.startsWith("1.0.") && void 0 === window.bcModSdk._shim10register)) { const e = window.bcModSdk, o = Object.freeze(Object.assign(Object.assign({}, e), { registerMod: (o, t, n) => o && "object" == typeof o && "string" == typeof o.name && "string" == typeof o.version ? e.registerMod(o.name, o.version, "object" == typeof t && !!t && !0 === t.allowReplace) : e.registerMod(o, t, n), _shim10register: !0 })); window.bcModSdk = o } return window.bcModSdk }(); return "undefined" != typeof exports && (Object.defineProperty(exports, "__esModule", { value: !0 }), exports.default = y), y }();

async function BondageClubScriptLoader() {
	"use strict";

	const w = window;

	if (w.BCSL_VERSION) {
		console.warn("BCSL already loaded. Skipping load.");
		return;
	}

	if (!w.bcModSdk) {
		console.warn("bcModSdk not found. Skipping load.");
		return;
	}

	const SDK = w.bcModSdk.registerMod(
		{
			name: "BCSL",
			version: BCSL_VERSION,
			fullName: "Bondage Club Script Loader",
			repository: "https://gitlab.com/Koalegy/BCSL/",
		},
		{
			allowReplace: false,
		}
	);

	w.BCSL_VERSION = BCSL_VERSION;
	//https://bcm.site.live/   ?
	const BCX_SOURCE = "https://raw.githubusercontent.com/Jomshir98/bondage-club-extended/b547db6453768f2c038ee51b5c1039780203cfc1/bcx.js",//Beta Next
		BCX_SOURCE_BETA = "https://jomshir98.github.io/bondage-club-extended/devel/bcx.js",
		EBCH_SOURCE = "https://e2466.gitlab.io/ebch/master/EBCH.js",//No Next
		EBCH_SOURCE_BETA = "https://e2466.gitlab.io/ebch/Beta/EBCHBeta.js",
		MBS_SOURCE = "https://bananarama92.github.io/MBS/main/mbs.js",//No Beta
		FBC_SOURCE = "https://sidiousious.gitlab.io/bce/bce-loader.user.js",//No Beta
		BCADCS_SOURCE = "https://dynilath.gitlab.io/SaotomeToyStoreVendor/ADCS/loader.user.js",//No Beta
		BCAR_SOURCE = "https://drbranestawm.github.io/BCAR/script/bcar.js",//Beta Next
		BCAR_SOURCE_BETA = "https://drbranestawm.github.io/BCAR/script/bcarBeta.js",
		BCT_SOURCE = "https://agicitag.github.io/BCTweaks/extension/bctLoader.user.js",//Beta Next
		BCT_SOURCE_BETA = "https://agicitag.github.io/BCTweaks/extension/bctLoader.user.js",
		ULTRABC_SOURCE = "https://tetris245.github.io/ultrabc.github.io/ULTRAbcloader.js",//No Beta
		LSCG_SOURCE = "https://littlesera.github.io/LSCG/bundle.js", //No Beta
		BCME_SOURCE = "https://BCMS7.github.io/BCM/BCME.user.js"; //We're going to ignore this one for now, too much douchey corporate crap for my script kiddie care.

	/** @type {Record<string, "none" | "external" | "stable" | "beta">} */
	const addonTypes = {
		BCX: "none",
		EBCH: "none",
		MBS: "none",
		FBC: "none",
		BCADCS: "none",
		BCAR: "none",
		BCT: "none",
		ULTRABC: "none",
		BCME: "none",
		LSCG: "none",
	};

	/** @type {Readonly<{Top: 11; OverrideBehaviour: 10; ModifyBehaviourHigh: 6; ModifyBehaviourMedium: 5; ModifyBehaviourLow: 4; AddBehaviour: 3; Observe: 0}>} */
	const HOOK_PRIORITIES = {
		Top: 11,
		OverrideBehaviour: 10,
		ModifyBehaviourHigh: 6,
		ModifyBehaviourMedium: 5,
		ModifyBehaviourLow: 4,
		AddBehaviour: 3,
		Observe: 0,
	};

	/** @type {Settings}*/
	let bcslPlayerSettings = {},
		/** @type {Settings}*/
		bcslCurrentSettings = {},
		/** @type {Settings}*/
		bcslStaticSettings = {};

	bcslPlayerSettings.dateStamp = bcslPlayerSettings.dateStampOrig = bcslCurrentSettings.dateStamp = bcslCurrentSettings.dateStampOrig = new Date();

	/** @type {Readonly<DefaultSettings>} */
	const settingsFrame = {
		pluginsPersistent: {
			label: "Use Global Settings.",
			defaultValue: false,
			sideEffects: (newValue) => {
				if (newValue === true) {
					SwapToPersistent("plugins");
				}
				else {
					SwapToPlayer("plugins");
				}
			},
			extraReturnValue: () => { return "false"; },
			category: "persistenceCheck",
			description: "Use the global settings set for the plugins menu.",
		},
		extensionsPersistent: {
			label: "Use Global Settings.",
			defaultValue: false,
			sideEffects: (newValue) => {
				if (newValue === true) {
					SwapToPersistent("extensions");
				}
				else {
					SwapToPlayer("extensions");
				}
			}, extraReturnValue: () => { return "false"; },
			category: "persistenceCheck",
			description: "Use the global settings set for the extensions menu.",
		},
		miscPersistent: {
			label: "Use Global Settings.",
			defaultValue: false,
			sideEffects: (newValue) => {
				if (newValue === true) {
					SwapToPersistent("misc");
				}
				else {
					SwapToPlayer("misc");
				}
			}, extraReturnValue: () => { return "false"; },
			category: "persistenceCheck",
			description: "Use the global settings set for the miscellaneous menu.",
		},
		bcadcs: {
			label: "Load BCADCS by Saki Saotome",
			defaultValue: false,
			sideEffects: (newValue) => {
				if (newValue) {
					loadExternalAddon("BCADCS", BCADCS_SOURCE).then((success) => {
						if (success) {
							addonTypes.BCADCS = "stable";
						}
					});
				}
				//debug("ebch", newValue);
			},
			extraReturnValue: () => { return "false"; },
			category: "plugins",
			description: "Load the latest stable version of Bondage Club Advanced Drone Control System.",
		},
		bcar: {
			label: "Load BCAR+ by DrBranestawm",
			defaultValue: false,
			sideEffects: (newValue) => {
				if (newValue) {
					if (newValue === "Beta") {
						loadExternalAddon("BCAR", BCAR_SOURCE_BETA).then((success) => {
							if (success) {
								addonTypes.BCAR = "beta";
							}
						});
					}
					else {
						loadExternalAddon("BCAR", BCAR_SOURCE).then((success) => {
							if (success) {
								addonTypes.BCAR = "stable";
							}
						});
					}
				}
				//debug("ebch", newValue);
			},
			extraReturnValue: () => { return "Beta"; },
			category: "plugins",
			description: "Load the latest stable or beta version of Bondage Club Auto React +.",
		},
		bct: {
			label: "Load BCT by agicitag",
			defaultValue: false,
			sideEffects: (newValue) => {
				if (newValue) {
					if (newValue === "Beta") {
						loadExternalAddon("BCT", BCT_SOURCE_BETA).then((success) => {
							if (success) {
								addonTypes.BCT = "beta";
							}
						});
					}
					else {
						loadExternalAddon("BCT", BCT_SOURCE).then((success) => {
							if (success) {
								addonTypes.BCT = "stable";
							}
						});
					}
				}
				//debug("ebch", newValue);
			},
			extraReturnValue: () => { return "Beta"; },
			category: "plugins",
			description: "Load the latest stable or beta version of Bondage Club Tools.",
		},
		bcx: {
			label: "Load BCX by Jomshir98 (no auto-update)",
			defaultValue: false,
			sideEffects: (newValue) => {
				if (newValue) {

					if (newValue === "Beta") {
						w.BCX_SOURCE = BCX_SOURCE_BETA;
						loadExternalAddon("BCX", BCX_SOURCE_BETA).then((success) => {
							if (success) {
								addonTypes.BCX = "beta";
							}
						});
					}
					else {
						loadExternalAddon("BCX", BCX_SOURCE).then((success) => {
							if (success) {
								addonTypes.BCX = "stable";
							}
						});
						w.BCX_SOURCE = BCX_SOURCE;
					}
				}
				//debug("bcx", newValue);
			},
			extraReturnValue: () => { return "Beta"; },
			category: "plugins",
			description: "Load the latest stable or veta version of Bondage Club Extended.",
		},
		/*bcme: { Seems pretty meh
			label: "Load BCME by ΩmegaX/Ash",
			defaultValue: false,
			sideEffects: (newValue) => {
				if (newValue) {
					loadExternalAddon("BCME", BCME_SOURCE).then((success) => {
						if (success) {
							addonTypes.BCME = "stable";
						}
					});
				}
				//debug("ebch", newValue);
			},
			extraReturnValue: () => { return "false"; },
			category: "plugins",
			description: "Load the latest stable version of Bondage Club Mods Essentials.",
		},*/
		ebch: {
			label: "Load EBCH by Elicia",
			defaultValue: false,
			sideEffects: (newValue) => {
				if (newValue) {

					if (newValue === "Beta") { //Depreciated
						loadExternalAddon("EBCH", EBCH_SOURCE_BETA).then((success) => {
							if (success) {
								addonTypes.EBCH = "beta";
							}
						});
					}
					else {
						loadExternalAddon("EBCH", EBCH_SOURCE).then((success) => {
							if (success) {
								addonTypes.EBCH = "stable";
							}
						});
					}
				}
				//debug("bcx", newValue);
			},
			extraReturnValue: () => { return "false"; },
			category: "plugins",
			description: "Load the latest stable version of Eli's Bondage Club Helper.",
		},

		fbc: {
			label: "Load FBC by Sidious",
			defaultValue: false,
			sideEffects: (newValue) => {
				if (newValue) {
					loadExternalAddon("FBC", FBC_SOURCE).then((success) => {
						if (success) {
							addonTypes.FBC = "stable";
						}
					});

				}
				localStorage.setItem(bcslFBCEarlyKey, JSON.stringify(newValue === settingsFrame["fbc"].extraReturnValue()));
			},
			extraReturnValue: () => { return "Start On Load"; },
			category: "plugins",
			description: "Load the latest stable version of For Better Club (Formerly BCE). Start on load allows FBC's password manager to be used.",
		},
		lscg: {
			label: "Load LCSG by Little Sera",
			defaultValue: false,
			sideEffects: (newValue) => {
				if (newValue) {
					loadExternalAddon("LSCG", LSCG_SOURCE).then((success) => {
						if (success) {
							addonTypes.LSCG = "stable";
						}
					});
				}
			},
			extraReturnValue: () => { return "false"; },
			category: "plugins",
			description: "Load the latest stable version of Little Sera's Club Games (Under rapid development, disable for a while if it causes errors)",
		},
		mbs: {
			label: "Load MBS by Rama",
			defaultValue: false,
			sideEffects: (newValue) => {
				if (newValue) {
					loadExternalAddon("MBS", MBS_SOURCE).then((success) => {
						if (success) {
							addonTypes.MBS = "stable";
						}
					});
				}
			},
			extraReturnValue: () => { return "false"; },
			category: "plugins",
			description: "Load the latest stable version of Maid's Bondage Scripts.",
		},
		ultrabc: {
			label: "Load ULTRAbc by Nemesea",
			defaultValue: false,
			sideEffects: (newValue) => {
				if (newValue) {
					loadExternalAddon("ULTRABC", ULTRABC_SOURCE).then((success) => {
						if (success) {
							addonTypes.ULTRABC = "stable";
						}
					});
				}
				//debug("ebch", newValue);
			},
			extraReturnValue: () => { return "false"; },
			category: "plugins",
			description: "Load the latest stable version of Ultra Bondage Club.",
		},
	};

	function settingsLoaded() {
		return Object.keys(bcslCurrentSettings).length > 2;
	}

	const bcslSettingsKeyBase = "bcsl.settings";
	const bcslPlayerSettingsKey = () => bcslSettingsKeyBase + `.${Player?.AccountName}`;
	const bcslPersistentSettingsKey = bcslSettingsKeyBase + ".static";

	const bcslFBCEarlyKey = bcslSettingsKeyBase + ".FBCLink.LoadEarly"

	/** @type {() => Promise<Settings>} */
	const bcslLoadSettings = async () => {

		bcslStaticSettings = JSON.parse(localStorage.getItem(bcslPersistentSettingsKey)) ?? {};

		if (JSON.parse(localStorage.getItem(bcslFBCEarlyKey)))
			loadExternalAddon("FBC", FBC_SOURCE).then((success) => {
				if (success) {
					addonTypes.FBC = "stable";
				}
			});

		await waitFor(() => !!Player?.AccountName);

		if (!settingsLoaded()) {
			// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
			bcslPlayerSettings = JSON.parse(localStorage.getItem(bcslPlayerSettingsKey()));
			bcslPlayerSettings ??= {}; //debug("no settings", key);

			LoadSettingsFromAccount();

			for (const setting in settingsFrame) { //Loop every setting
				if (!(setting in bcslPlayerSettings)) //Ensure full values for player
					bcslPlayerSettings[setting] = settingsFrame[setting].defaultValue;
				if (!(setting in bcslStaticSettings) && settingsFrame[setting].category !== "persistenceCheck") //Ensure full values for statics
					bcslStaticSettings[setting] = settingsFrame[setting].defaultValue;

				if (bcslPlayerSettings[setting] === "beta")
					bcslPlayerSettings[setting] = "Beta";
				if (bcslStaticSettings[setting] === "beta")
					bcslStaticSettings[setting] = "Beta";

				if (settingsFrame[setting].category !== "persistenceCheck") //If setting can be static
					if (bcslPlayerSettings[settingsFrame[setting].category + "Persistent"] === true) { //If setting category is set to persisent
						bcslCurrentSettings[setting] = bcslStaticSettings[setting]; //Set current to persisent
						continue; //Jump out
					}
				//Else
				bcslCurrentSettings[setting] = bcslPlayerSettings[setting]; //Set current to players
			}

			if (JSON.parse(localStorage.getItem(bcslFBCEarlyKey)))
				bcslCurrentSettings["fbc"] = settingsFrame["fbc"].extraReturnValue();
		}
		return bcslCurrentSettings;
	};

	const bcslSaveSettings = () => {
		localStorage.setItem(bcslPersistentSettingsKey, JSON.stringify(bcslStaticSettings));
		localStorage.setItem(bcslPlayerSettingsKey(), JSON.stringify(bcslPlayerSettings));

		SaveSettingsToAccount();
	};

	function postSettings() {
		//debug("handling settings side effects");
		for (const [k, v] of Object.entries(bcslCurrentSettings)) {
			if (k in settingsFrame) {
				// @ts-ignore
				settingsFrame[k].sideEffects(v);
			}
		}
		if (JSON.parse(localStorage.getItem(bcslFBCEarlyKey)))
			bcslCurrentSettings["fbc"] = settingsFrame["fbc"].extraReturnValue();

		bcslSaveSettings();
	}
	/** @type {(key: string, val: SettingValue) => void} */
	function SetSetting(key, val) {
		if (key === "fbc" && val === settingsFrame[key].extraReturnValue()) {
			localStorage.setItem(bcslFBCEarlyKey, JSON.stringify(true));
			bcslCurrentSettings[key] = settingsFrame[key].extraReturnValue();
			val = true;
		}
		else
			bcslCurrentSettings[key] = val;
		if (settingsFrame[key].category !== "persistenceCheck" && bcslPlayerSettings[settingsFrame[key].category + "Persistent"] === true) { //If setting can be static and is persistent set currently
			bcslStaticSettings[key] = val;
			bcslStaticSettings.dateStamp = new Date();
			localStorage.setItem(bcslPersistentSettingsKey, JSON.stringify(bcslStaticSettings));
		}
		else {
			bcslPlayerSettings[key] = val;
			bcslPlayerSettings.dateStamp = new Date();
		}

		settingsFrame[key].sideEffects(bcslCurrentSettings[key]);
	}

	/** @type {(category: SettingsCategory) => void} */
	function SwapToPersistent(category) {
		for (const setting in settingsFrame) { //Loop every setting
			if (settingsFrame[setting].category === category) // If is to swap
				bcslCurrentSettings[setting] = bcslStaticSettings[setting]; //Set to static.
		}

		if (JSON.parse(localStorage.getItem(bcslFBCEarlyKey)))
			bcslCurrentSettings["fbc"] = settingsFrame["fbc"].extraReturnValue();
	}

	/** @type {(category: SettingsCategory) => void} */
	function SwapToPlayer(category) {
		for (const setting in settingsFrame) { //Loop every setting
			if (settingsFrame[setting].category === category) // If is to swap
				bcslCurrentSettings[setting] = bcslPlayerSettings[setting]; //Set to player.
		}

		if (JSON.parse(localStorage.getItem(bcslFBCEarlyKey)))
			bcslCurrentSettings["fbc"] = settingsFrame["fbc"].extraReturnValue();
	}

	function SaveSettingsToAccount() {
		Player.OnlineSettings.BCSL = LZString.compressToBase64(JSON.stringify(bcslPlayerSettings));
		Player.OnlineSettings.BCSLPersistent = LZString.compressToBase64(JSON.stringify(bcslStaticSettings));
		ServerAccountUpdate.QueueData({ OnlineSettings: Player.OnlineSettings, });
	}

	function LoadSettingsFromAccount() {
		/** @type {Settings} */
		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		let onlineSettings = JSON.parse(LZString.decompressFromBase64(Player.OnlineSettings.BCSL) || null);
		if (onlineSettings?.dateStamp >= bcslPlayerSettings?.dateStamp || (typeof bcslPlayerSettings?.dateStamp === "undefined" && typeof onlineSettings?.dateStamp !== "undefined")) {
			bcslPlayerSettings = onlineSettings;
		}

		onlineSettings = JSON.parse(LZString.decompressFromBase64(Player.OnlineSettings.BCSLPersistent) || null);
		if (onlineSettings?.dateStamp >= bcslStaticSettings?.dateStamp || (typeof bcslStaticSettings?.dateStamp === "undefined" && typeof onlineSettings?.dateStamp !== "undefined")) {
			bcslStaticSettings = onlineSettings;
		}
	}

	const incompleteFunctions = [];

	/** @type {(func: () => (Promise<unknown> | unknown), label: string) => Promise<void>} */
	const registerFunction = async (func, label) => {
		incompleteFunctions.push(label);
		try {
			const ret = func();
			if (ret instanceof Promise) {
				await ret;
			}
			incompleteFunctions.splice(incompleteFunctions.indexOf(label), 1);
		} catch (err) {
			/** @type {Error} */
			// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
			const e = err;
		}
	};

	await registerFunction(bcslLoadSettings, "bcslLoadSettings");
	registerFunction(postSettings, "postSettings");
	registerFunction(settingsPage, "settingsPage");

	// Load BCX
	/** @type {(addon: "BCX" | "EBCH" | "MBS" | "FBC" | "BCADCS" | "BCAR" | "BCT" | "ULTRABC" | "BCME" | "LSCG", source: string) => Promise<boolean>} */
	async function loadExternalAddon(addon, source) {
		//await waitFor(settingsLoaded);

		if (bcModSdk.getModsInfo().some((mod) => mod.name === addon)) {
			addonTypes[addon] = "external";
			return false;
		}

		await fetch(source)
			.then((resp) => resp.text())
			.then((resp) => {
				resp = resp.replace(
					/sourceMappingURL=.*?.map/u,
					`sourceMappingURL=${source}.map`
				);
				eval?.(resp);
			});
		return true;
	}


	// Create settings page
	async function settingsPage() {
		await waitFor(() => !!PreferenceSubscreenList);

		const settingsPerPage = 8,
			settingsYIncrement = 70,
			settingsMenuNavButtonSize = 90,
			settingsGenericRowItemSize = 64,
			settingsManuNavButtonSpacing = 105;

		/** @type {Rectangle} */
		const settingsFullDrawingBounds = { X: 300, Y: 125, Width: 1605, Height: 0 },
			/** @type {Rectangle} */
			settingsDrawingBounds = { X: settingsFullDrawingBounds.X, Y: 225, Width: settingsFullDrawingBounds.Width, Height: 0 },
			/** @type {Rectangle} */
			settungsExitButtonRectangle = { X: settingsFullDrawingBounds.X + settingsFullDrawingBounds.Width - settingsMenuNavButtonSize, Y: 75, Width: settingsMenuNavButtonSize, Height: settingsMenuNavButtonSize },
			/** @type {Rectangle} */
			settungsNextButtonRectangle = { X: settungsExitButtonRectangle.X - settingsManuNavButtonSpacing, Y: settungsExitButtonRectangle.Y, Width: settingsMenuNavButtonSize, Height: settingsMenuNavButtonSize },
			/** @type {Rectangle} */
			settungsPageNumberRectangle = { X: settungsNextButtonRectangle.X - settingsManuNavButtonSpacing, Y: settungsNextButtonRectangle.Y + settingsMenuNavButtonSize / 2, Width: settingsMenuNavButtonSize, Height: settingsMenuNavButtonSize },
			/** @type {Rectangle} */
			EntryTextDrawingRectangle = { X: settingsDrawingBounds.X, Y: -1, Width: settingsDrawingBounds.Width / 2, Height: -1 },
			/** @type {Rectangle} */
			settingsLabelButtonRectangle = { X: settingsDrawingBounds.X, Y: -1, Width: 400, Height: settingsGenericRowItemSize }

		const settingsDualCheckboxBox1X = EntryTextDrawingRectangle.X + EntryTextDrawingRectangle.Width + settingsManuNavButtonSpacing,
			settingsDualCheckboxBox2X = EntryTextDrawingRectangle.X + EntryTextDrawingRectangle.Width + settingsManuNavButtonSpacing * 2 + settingsGenericRowItemSize;

		const settingsPageCount = (category) =>
			Math.ceil(
				Object.values(settingsFrame).filter((v) => v.category === category)
					.length / settingsPerPage
			);

		let currentPageNumber = 0;

		/** @type {SettingsCategory | null} */
		let currentCategory = null;
		let currentSetting = "";
		/**
		 * Excludes hidden
		 * @type {SettingsCategory[]}
		 */
		const settingsCategories = [
			"plugins",
			"extensions",
			//"misc",
		];
		const settingCategoryLabels = {
			plugins: "Plugin Loading",
			extensions: "BCPL Extensions",
			//misc: "Miscellaneous",
		};

		const currentDefaultSettings = (category) =>
			Object.entries(settingsFrame).filter(
				([, v]) => v.category === category && v.defaultValue === !!v.defaultValue
			);

		w.PreferenceSubscreenBCSLSettingsLoad = function () {
			currentPageNumber = 0;
		};
		w.PreferenceSubscreenBCSLSettingsExit = function () {
			bcslSaveSettings();
			PreferenceSubscreen = "";
			PreferenceMessage = "";
		};
		w.PreferenceSubscreenBCSLSettingsRun = function () {
			w.MainCanvas.getContext("2d").textAlign = "left";
			DrawText(
				displayText("Bondage Club Plugin Loader Settings"),
				settingsDrawingBounds.X,
				125,
				"Black",
				"Gray"
			);

			//Draws the exit button
			DrawButton(settungsExitButtonRectangle.X, settungsExitButtonRectangle.Y, settungsExitButtonRectangle.Width, settungsExitButtonRectangle.Height, "", "White", "Icons/Exit.png");

			//If in a category screen
			if (currentCategory) {
				if (currentCategory === "plugins") //If in the plugins screen
				{
					let y = settingsDrawingBounds.Y;
					for (const [settingName, defaultSetting] of currentDefaultSettings(currentCategory).slice(currentPageNumber * settingsPerPage, currentPageNumber * settingsPerPage + settingsPerPage)) {
						DrawTextFit(
							displayText(defaultSetting.label + ":"),
							EntryTextDrawingRectangle.X,
							y + settingsGenericRowItemSize / 2,
							EntryTextDrawingRectangle.Width,
							currentSetting === settingName ? "Red" : "Black",
						);

						DrawCheckbox(
							settingsDualCheckboxBox1X,
							y,
							settingsGenericRowItemSize,
							settingsGenericRowItemSize,
							"",
							bcslCurrentSettings[settingName] == true,
							false,
							currentSetting === settingName ? "Red" : "Black"
						);

						DrawCheckbox(
							settingsDualCheckboxBox2X,
							y,
							settingsGenericRowItemSize,
							settingsGenericRowItemSize,
							settingsFrame[settingName].extraReturnValue() !== "false" ? settingsFrame[settingName].extraReturnValue() : "", //Text
							bcslCurrentSettings[settingName] === settingsFrame[settingName].extraReturnValue(), // Checked
							settingsFrame[settingName].extraReturnValue() === "false", // Greyed
							currentSetting === settingName ? "Red" : "Black"
						);
						y += settingsYIncrement;
					}

					//Draw the lower tooltip advisor
					DrawText(
						displayText("Click on a setting to see its description"),
						settingsDrawingBounds.X,
						160,
						"Gray",
						"Silver"
					);

					//Draw the tooltip itself
					if (currentSetting in settingsFrame) {
						drawTooltip(
							settingsDrawingBounds.X,
							830,
							1400,
							displayText(settingsFrame[currentSetting].description),
							"left"
						);
					}

					//Draw the page number
					DrawTextFit(
						`${currentPageNumber + 1} / ${settingsPageCount(currentCategory)}`,
						settungsPageNumberRectangle.X,
						settungsPageNumberRectangle.Y,
						settungsPageNumberRectangle.Width,
						"Black",
					);

					//Draw the next page dialouge
					DrawButton(settungsNextButtonRectangle.X, settungsNextButtonRectangle.Y, settungsNextButtonRectangle.Width, settungsNextButtonRectangle.Height, "", "White", "Icons/Next.png");
				}
				else if (currentCategory === "extensions") {
					let y = settingsDrawingBounds.Y;
					for (const [settingName, defaultSetting] of currentDefaultSettings(currentCategory)
						.slice(currentPageNumber * settingsPerPage, currentPageNumber * settingsPerPage + settingsPerPage)) {
						DrawCheckbox(
							settingsDrawingBounds.X,
							y,
							settingsGenericRowItemSize,
							settingsGenericRowItemSize,
							displayText(defaultSetting.label),
							!!bcslCurrentSettings[settingName],
							false,
							currentSetting === settingName ? "Red" : "Black"
						);
						y += settingsYIncrement;
					}

					//Draw the lower tooltip advisor
					const Lines = ["Currently Empty, when I add content beyond the basic plugin manager stuff", "this will be a menu to enable each addition modularly", "to avoid cluttering the menu up with crap you'll never use."];
					let index = 0;

					for (const line of Lines) {
						DrawText(
							displayText(line),
							settingsDrawingBounds.X,
							160 + settingsGenericRowItemSize * index * 0.6,
							"Gray",
							"Silver"
						);
						index++;
					}
				}
			} else { //When not in a category screen
				let y = settingsDrawingBounds.Y;
				for (const category of settingsCategories) { //Loop throught category
					DrawButton(settingsLabelButtonRectangle.X, y, settingsLabelButtonRectangle.Width, settingsLabelButtonRectangle.Height, "", "White"); //Draw button border
					DrawTextFit( //Draw category names
						displayText(settingCategoryLabels[category]),
						310,
						y + 32,
						380,
						"Black"
					);

					DrawCheckbox(
						settingsLabelButtonRectangle.X + settingsLabelButtonRectangle.Width + settingsManuNavButtonSpacing, //X
						y, //Y
						settingsGenericRowItemSize, //Width
						settingsGenericRowItemSize, //Height
						"", //Text
						bcslCurrentSettings[category + "Persistent"] === true, // Checked?
						false, // Greyed?
						"Black" //Text Colour
					);

					y += settingsYIncrement; //increment spacing
				}
			}
			w.MainCanvas.getContext("2d").textAlign = "center";
		};
		// eslint-disable-next-line complexity
		w.PreferenceSubscreenBCSLSettingsClick = function () {
			let y = settingsDrawingBounds.Y;
			if (MouseIn(1815, 75, 90, 90)) {
				if (currentCategory === null) {
					PreferenceSubscreenBCSLSettingsExit();
				} else {
					currentCategory = null;
				}
			} else if (currentCategory !== null) {
				if (currentCategory === "plugins") { //If in the plugins screen
					if (MouseIn(settungsNextButtonRectangle.X, settungsNextButtonRectangle.Y, settungsNextButtonRectangle.Width, settungsNextButtonRectangle.Height)) {
						currentPageNumber += 1;
						currentPageNumber %= settingsPageCount(currentCategory);
					} else {
						for (const [settingName, defaultSetting] of currentDefaultSettings(currentCategory).slice(currentPageNumber * settingsPerPage, currentPageNumber * settingsPerPage + settingsPerPage)) {
							if (MouseIn(settingsDualCheckboxBox1X, y, settingsGenericRowItemSize, settingsGenericRowItemSize)) {
								SetSetting(settingName, bcslCurrentSettings[settingName] !== true);
							} else if (MouseIn(settingsDualCheckboxBox2X, y, settingsGenericRowItemSize, settingsGenericRowItemSize) && settingsFrame[settingName].extraReturnValue() !== "false") {
								if (bcslCurrentSettings[settingName] === settingsFrame[settingName].extraReturnValue())
									SetSetting(settingName, false);
								else
									SetSetting(settingName, settingsFrame[settingName].extraReturnValue());
							} else if (MouseIn(EntryTextDrawingRectangle.X, y, EntryTextDrawingRectangle.Width, settingsGenericRowItemSize)) {
								currentSetting = settingName;
								//debug("currentSetting", currentSetting);
							}
							y += settingsYIncrement;
						}
					}
				}
				else if (currentCategory === "extensions") {
					if (MouseIn(1815, 180, 90, 90)) {
						currentPageNumber += 1;
						currentPageNumber %= settingsPageCount(currentCategory);
					} else {
						for (const [settingName, defaultSetting] of currentDefaultSettings(
							currentCategory
						).slice(
							currentPageNumber * settingsPerPage,
							currentPageNumber * settingsPerPage + settingsPerPage
						)) {
							if (MouseIn(300, y, settingsGenericRowItemSize, settingsGenericRowItemSize)) {
								SetSetting(settingName, !bcslCurrentSettings[settingName]);
							} else if (MouseIn(364, y, 1000, settingsGenericRowItemSize)) {
								currentSetting = settingName;
								//debug("currentSetting", currentSetting);
							}
							y += settingsYIncrement;
						}
					}
				}
			} else {
				for (const category of settingsCategories) {
					if (MouseIn(300, y, 400, settingsGenericRowItemSize)) {
						currentCategory = category;
						currentPageNumber = 0;
						break;
					}
					if (MouseIn(settingsLabelButtonRectangle.X + settingsLabelButtonRectangle.Width + settingsManuNavButtonSpacing, y, settingsGenericRowItemSize, settingsGenericRowItemSize))
						SetSetting(category + "Persistent", bcslCurrentSettings[category + "Persistent"] === false);
					y += settingsYIncrement;
				}
			}
		};

		SDK.hookFunction(
			"DrawButton",
			HOOK_PRIORITIES.ModifyBehaviourMedium,
			(args, next) => {
				// 7th argument is image URL
				switch (args[6]) {
					case "Icons/BCSLSettings.png":
						args[6] = Images.SettingsIcon;
						break;
					default:
						break;
				}
				return next(args);
			}
		);

		SDK.hookFunction(
			"TextGet",
			HOOK_PRIORITIES.ModifyBehaviourHigh,
			(args, next) => {
				switch (args[0]) {
					case "HomepageBCSLSettings":
						return displayText("BCSL Settings");
					default:
						return next(args);
				}
			}
		);
		PreferenceSubscreenList.push("BCSLSettings");

		/** @type {(e: KeyboardEvent) => void} */
		function keyHandler(e) {
			if (e.key === "Escape" && currentCategory !== null) {
				currentCategory = null;
				e.stopPropagation();
				e.preventDefault();
			}
		}

		document.addEventListener("keydown", keyHandler, true);
		document.addEventListener("keypress", keyHandler, true);
	}

	/** @type {(x: number, y: number, width: number, text: string, align: "left" | "center") => void} */
	function drawTooltip(x, y, width, text, align) {
		const canvas = w.MainCanvas.getContext("2d");
		const bak = canvas.textAlign;
		canvas.textAlign = align;
		canvas.beginPath();
		canvas.rect(x, y, width, 65);
		canvas.fillStyle = "#FFFF88";
		canvas.fillRect(x, y, width, 65);
		canvas.fill();
		canvas.lineWidth = 2;
		canvas.strokeStyle = "black";
		canvas.stroke();
		canvas.closePath();
		DrawTextFit(
			text,
			align === "left" ? x + 3 : x + width / 2,
			y + 33,
			width - 6,
			"black"
		);
		canvas.textAlign = bak;
	}

	function displayText(original, replacements = {}) {
		let text = original;
		for (const [key, val] of Object.entries(replacements)) {
			while (text.includes(key)) {
				text = text.replace(key, val);

			}
		}
		return text;
	}

	/** @type {(func: () => boolean, cancelFunc?: () => boolean) => Promise<boolean>} */
	async function waitFor(func, cancelFunc = () => false) {
		while (!func()) {
			if (cancelFunc()) {
				return false;
			}
			// eslint-disable-next-line no-await-in-loop
			await sleep(10);
		}
		return true;
	}

	/** @type {(ms: number) => Promise<void>} */
	function sleep(ms) {
		// eslint-disable-next-line no-promise-executor-return
		return new Promise((resolve) => setTimeout(resolve, ms));
	}

	const Images = { SettingsIcon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFYAAABWCAYAAABVVmH3AAAACXBIWXMAAAsTAAALEwEAmpwYAAAF5GlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNi4wLWMwMDYgNzkuZGFiYWNiYiwgMjAyMS8wNC8xNC0wMDozOTo0NCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIiB4bWxuczpwaG90b3Nob3A9Imh0dHA6Ly9ucy5hZG9iZS5jb20vcGhvdG9zaG9wLzEuMC8iIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIDIyLjQgKFdpbmRvd3MpIiB4bXA6Q3JlYXRlRGF0ZT0iMjAyMy0wMy0wM1QyMzoyNzozMiswMTowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAyMy0wMy0wM1QyMzozMDo1NiswMTowMCIgeG1wOk1vZGlmeURhdGU9IjIwMjMtMDMtMDNUMjM6MzA6NTYrMDE6MDAiIGRjOmZvcm1hdD0iaW1hZ2UvcG5nIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjQ1MjFkMWUyLTg0NzktMmE0OS05NGNjLTllMjNmMjU1MmUzZiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo5ODY3YWY3Ni1kZDIwLWJlNGQtYjUwZi1jNDg4ZTQwZGE4ODMiIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo5ODY3YWY3Ni1kZDIwLWJlNGQtYjUwZi1jNDg4ZTQwZGE4ODMiIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiIHBob3Rvc2hvcDpJQ0NQcm9maWxlPSJzUkdCIElFQzYxOTY2LTIuMSI+IDx4bXBNTTpIaXN0b3J5PiA8cmRmOlNlcT4gPHJkZjpsaSBzdEV2dDphY3Rpb249ImNyZWF0ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6OTg2N2FmNzYtZGQyMC1iZTRkLWI1MGYtYzQ4OGU0MGRhODgzIiBzdEV2dDp3aGVuPSIyMDIzLTAzLTAzVDIzOjI3OjMyKzAxOjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgMjIuNCAoV2luZG93cykiLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249InNhdmVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjQ1MjFkMWUyLTg0NzktMmE0OS05NGNjLTllMjNmMjU1MmUzZiIgc3RFdnQ6d2hlbj0iMjAyMy0wMy0wM1QyMzozMDo1NiswMTowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIyLjQgKFdpbmRvd3MpIiBzdEV2dDpjaGFuZ2VkPSIvIi8+IDwvcmRmOlNlcT4gPC94bXBNTTpIaXN0b3J5PiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pk3BX+sAAAa4SURBVHja7ZxpbFRFHMB/RVq6UO5DzlYFwQOVSxBDKipiFEEMGmMFbEAxGNR4JShqBRKVDxLAqEFCBJGjQfCDSMUj4AEiyKEColBBOcJRVERaCsr6Yf4bHq+779judt+89yaZtOnMm3n76+x/5n8N0WiURFXTUg+IpHJAK0YJ2WkCtj3QSaDZlcbAwBBs/HIBcANQAkwDngC6OQTbA5gYgq35NR4LbAf2AZOBfJdjTAaesWjPAnKCBLYvsBGIAjOAtkmM0Rk4BFyeoH0oMAFoERSwdwKVwHHgtiTHaAXsABYmaB8CfAG0DooouBaoAk4AvZIcoxewTVZrqwQyewdQHBQZ20CARIHRSTzfEHgWOCX/nL4J+l0K/Av0DgrY8QJ1s8vnsoBRwE55/jcbaIOk341BAbtGPvBjLp7pD6yV56LAcqCdzTNXS98XgwA2AuyXD9zHQf/6wGvAGXnmD2Ckw7nygKPAYaCp38E2BSoEUl8HsnSZYZWuEbnppiyXZ6f4HWw2sFs+7CQbebrYAPUt2eXdlsHAWaBaRIOvZWypwNorOn688qgB6vRazrdaxtkgosW3YPsboJXGaW8C/C7ta2WV16YUAv/JeBP8riC8aYA717S53G9oG5Ki+ZbKeIecbmS6go0AnxoAbgGGSdsU+VuFW/3e5uh1WsZ9yO+2gjzTrh87n8Zk4h63FimbslnG/SwI1q0s2aiOmABHRV3tksK55sq4u538w/xi6M4XEXDUBHengK+fgjlKZMyDTuSs31wz7YBxcVbwRmBAijbMvUBu0MDGSkw5OGUSDcW1GPNrGeebIMjYROV5gfCrHLk2GQCPSmK8i8Wobqfx+R7sGIHwp9hvcw2bTxXQM0ltrxLoGGSw9wmIY6az7Hz5+yoXYz1uWO2TnD7k9xX7vcn40kS0p2pUzIFd6QP8I2OtcGPI8buMXRCnbaq0jXGggGw3bFh5frduuZGJ8YwmVzm0ek2WflsJkJfWzsC9T+yo8WIFWogomG0xRg5QLv2uTOYl/Ah2pIVJMWbAOQa8bnO8iopSQAhWrdZtNhb/iGhmVs7I3gJ2aAhWlYkCxCrArTlwALjGok+BmAkHhGDhVoPh2+pYdJlsSA0s+tQHdgHDgw72ZuAkMFNMiVZlGMq5aFemolzngQU7HvgRKHLY/zmgn4N+ETk5FPgNbD3ZgPrJ2bOL6OktgUZAB1Rw8UtAGxfj3u2ib1uU87KBgG4sZ9p8oKvI6etQAc5agG0t+nxMRz8rJsC/Ub6sg4afa4F18nMNynXyMfAhygm4SDSwd4B58vvbht8XA++LyroK+BwVurkOWC9q8S5UrNchOaqdkJNH7P1OA3OMdlqvgn2Dmq4WHeojXge7UVOw72UabFugOyqpomscA8d3moJdlAmwnYGngU/EAB2LLqkW+TVPNpZczg+51KkurEuwDYEXgL8cvtwmzoVqhmATgO1kcMIFodYJ2PZyVDFPfkAO3k/KLloiR6MzIVh7sNnAStOkx8Xw3CqBDO4hwj8EawH2AdOEh7GPwI6VEo1Xb1rBRjiXLhRFpfQMdHmO3RKCrQl2iGmyWUkoCBtCsDXBvmqY6BSJc1T9qHmlFexyw0TfBkylTSvYLzk/EDgEmyKwX4Vg0wP2A8NE60OwqQM7zTBRZTzregg2ObB3mCabGR63UgO2kSGYLCpaVGGoIKRGpR1rmnC/2AKcBl2cDsHGB5sDlJkmrUAF8Ta0CKQoDY0wzsyGP8SZvBx4FxXD+pRsdmXi+dTdbLigrgzdF2m8ESVTZ9elayYCvOzCNbMZ+ElTsFMy4UwskK//egmjPCMOxZPiTFwB3CVG8qmagh2dafd3SwkZ6ibhOo1M7UWagu3p9YCNjuLK0QnqVgw5u14Oilumq3z1OtjhGkGtAq7QBWyWRPzpAHaOboHHt2gAtYI4WY46RHTP8DjYcbqGyud4OExpvu45CO05d0OcV+pqLG6i1ym54xKTnTeT9SOgmdXL6pY100YC6DIJdTo+vcWoHipb5mgdA90D3Ov0JXXO8+qASoQ7kmag5aiUp2ZuXs4PCXRtUNfirURdFlldS5CVqPSjpcAIXF4A4Sew5nIhKvFuMCqnwUkdAdyEupugeSpewmtgC4GHk6zjxIlZjLqzoAh16Y7TWoS6OqoYeLCW79HdS2CzZYPwg4um1Gsr9nYfOBV/9tqKjZVBqJxZHaGWoxICPbt5DXLhfPRK/QVDkLWXTwXXaxRqVCYqtzbHrTzgHlEjFwJLUGnwma5LJDjjFdSVKek/boU1+RpCSFP9HwwY3ZbseGEOAAAAAElFTkSuQmCC", }
}

BondageClubScriptLoader();
