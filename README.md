Heeeeeeeya 💖, BCSL or Bondage Club Script Loader is a fun project I've been working on to help with all the options we have for scripts on BC.
If you think I'm missing a script that should really be in here, or just have a general suggestion for the script, feel free to open an issue or PR it.

Link for Tampermonkey: https://gitlab.com/Koalegy/BCSL/-/raw/main/bcsl.user.js

- With Love 💝, Koalegy.
